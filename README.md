# proj3-JSA
Vocabulary anagrams game for primary school English Language Learners (ELL)

## Authors

Initial version by M Young; Docker version added by R Durairajan; revised by Jared Knofczynski (jknofczy@uoregon.edu) for CIS 322.

## Running the program

To run the program, execute the following in the `/vocab` directory:

```
docker build -t jknofczy-proj3:latest .
docker run -dp 5000:5000 jknofczy-proj3
```

Or run the program using `./run.sh`.

## Extra Credit Information

I believe I have successfully implemented all three portions of the
extra credit for this assignment;

- Invalid characters/letters not in the jumble should be automatically removed

- Letters in the current attempt are crossed out in the jumble as they are used

- Partially built words are dynamically highlighted as attempts are made.

Tips for implementing these components were found at the following:

- https://stackoverflow.com/questions/52187217/how-to-allow-only-english-letters-in-input-fields

- https://stackoverflow.com/questions/8644428/how-to-highlight-text-using-javascript

## Grading Rubric

* If your code works as expected: 100 points. This includes:
	* AJAX in the frontend (vocab.html)
	* Logic in the backend (flask_vocab.py)
	* Frontend to backend interaction (with correct requests/responses) between vocab.html and flask_vocab.py

* If the JQuery logic is not working, 30 points will be docked off.

* If the html file (replacing the original form interaction, i.e., replacing the button with JQuery code) is wrong or is missing in the appropriate location, 30 points will be docked off.

* If none of the functionalities work, 40 points will be given assuming
    * the credentials.ini is submitted with the correct URL of your repo,
    * the Dockerfile builds without any errors, and

* If the Dockerfile doesn't build or is missing, 20 points will be docked off.

* If credentials.ini is missing, 0 will be assigned.
